import socket
from _thread import *

#To use the server open any browser and write localhost/hello


#Specify the host address and port which will be used
SERVER_ADDRESS = (HOST, PORT) = '', 80
#Set a maximum size on how people can connect at once
REQUEST_QUEUE_SIZE = 10

#Path to a html file that will be sent to the client users
path = "./website/hello.html"

#Function that handles client requests
def handle_request(client_connection):
    #Receive the request coming from the client with a buffer size of 1024
    request= client_connection.recv(1024)

    #Read the information stores in the html file such that is is ready to be sent
    HtmlFile = open(path, 'r', encoding='utf-8')
    source_code = HtmlFile.read() 
    
    #If the reqest from the client contains a HTTP get to the specific file then we send it and close the connection
    if "GET /hello HTTP/1.1" in request.decode('utf-8'):
        http_response = bytes(source_code, encoding="utf-8")
        client_connection.sendall(http_response)
        client_connection.close()
    #If we don't get a http get request or the file is wrong then we send a 404 to the client and closes the connection
    else:
        http_response = b"HTTP/1.1 404"
        client_connection.sendall(http_response)
        client_connection.close()

#Function that will listen for incomming requests
def serve_forever():
    listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #Specify that the socket uses TCP
    listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #Allows us to use the same address
    listen_socket.bind(SERVER_ADDRESS) #Binds the socket to a specific address (in this case localhost)
    listen_socket.listen(REQUEST_QUEUE_SIZE)
    print('Serving HTTP on port {port} ...'.format(port=PORT))

    #When a client connects the server we accept and then create a thread which will handle the specific connection
    while True:
        client_connection, client_address = listen_socket.accept()
        start_new_thread(handle_request,(client_connection,))
        

#Server starts here and calls the serve_forever functions
if __name__ == '__main__':
    print("This is a test")
    serve_forever()
